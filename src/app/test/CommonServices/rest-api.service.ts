import { Injectable, NgZone } from '@angular/core';

import { StorageService } from './storage.service';

import { RequestOptionsArgs, Response, Headers,  ResponseContentType, Http } from '@angular/http';
import { Router } from '@angular/router';

import { Observable, throwError } from 'rxjs';
import {map,  catchError} from "rxjs/operators";
import { Settings } from '../Settings';
@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  
  constructor(private http: Http, private zone: NgZone, private router: Router,
    private storageService: StorageService
    
    ) {
  }

  private prependApiUrl(url: string): string {
     return Settings.BASE_URL + '/' + url;
  }

  get(url: string,  options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
    
    return this.http.get(this.prependApiUrl(url), options)
            .pipe(map((Response) => this.handleSuccess(Response)) , catchError((error) => this.handleError(error))
            );
  }

  post(url: string, body: any,  options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
 
    return this.http.post(this.prependApiUrl(url), body, options).pipe(map((Response) => this.handleSuccess(Response))
      , catchError((error) => this.handleError(error)));
  }

  put(url: string, body?: any, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
 
    return this.http.put(this.prependApiUrl(url), body, options).pipe(map((Response) => this.handleSuccess(Response))
      , catchError((error) => this.handleError(error)));
  }

  delete(url: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
  
    return this.http.delete(this.prependApiUrl(url), options).pipe(map((Response) => this.handleSuccess(Response))
      , catchError((error) => this.handleError(error)));
  }

  patch(url: string, body: any, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
  
    return this.http.patch(this.prependApiUrl(url), body, options);
  }

  head(url: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
     return this.http.head(this.prependApiUrl(url), options);
  }

  options(url: string, options: RequestOptionsArgs = { headers: this.getHeaders() }): Observable<Response> {
  
    return this.http.options(this.prependApiUrl(url), options);
  }

  excel(url: string, fileName: string,body: any,
    options: RequestOptionsArgs = {
      headers: this.getHeaders(),
      responseType: ResponseContentType.Blob
    }) {
  
    return this.http.post(this.prependApiUrl(url),body, options).subscribe(data =>
      this.downloadFile(data, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', fileName)),
      error => this.handleError(error);
  }

  pdf(url: string, fileName: string,
    options: RequestOptionsArgs = {
      headers: this.getHeaders(),
      responseType: ResponseContentType.Blob
    }) {
    //this.showLoader(loader);
    return this.http.get(url, options).subscribe(data => this.downloadFile(data, 'application/pdf', fileName)),
      error => this.handleError(error);
  }

  image(url: string, fileName: string, options: RequestOptionsArgs =
    { headers: this.getHeaders(), responseType: ResponseContentType.Blob }) {
    //this.showLoader(loader);
    return this.http.get(url, options).subscribe(data => this.downloadFile(data, this.getContentType(fileName), fileName)),
      error => this.handleError(error);
  }
  authToken:string;
  private getHeaders(): Headers {
    // const defaultLanguage: any = this.storageService.getItem(Settings.DEFAULTLANGUAGE);
    const headers = new Headers();
    headers.append('Accept-Language', 'en-US');
    headers.append('Content-Type', 'application/json');
    // headers.append('x-app-type', 'PSWWEBAPP');
    this.authToken = this.storageService.getItem(Settings.Auth_Token);
    if(this.authToken!=null){
      headers.append('authorization', "Bearer "+this.authToken );
    }
    // headers.append('Accept', AppSettings.HEADER_CONTENT_TYPE);
    return headers;
  }

  private handleError(error: Response | any) {
   
    const body = error.json() || '';
    if (error.status === 400) {
      if (error instanceof Response) {
        //const err = body.error || JSON.stringify(body);
        return throwError(body);
        // return Observable.throw(body);
      }
    } else if (error.status === 500) {
      if (error instanceof Response) {
        //const err = body.error || JSON.stringify(body);
        return throwError(body);
        // return Observable.throw(body);
      }
    } else if (error.status === 401) {
      // console.log(body);
      // console.log(error.status);
      this.storageService.removeAll();
      this.router.navigate(['/login']);
    }else if(error.status == 409){

      if (error instanceof Response) {
        //const err = body.error || JSON.stringify(body);
        return throwError(body);
        // return Observable.throw(body);
      }

    } else if (error.status === 403) {
      if (error instanceof Response) {
        //const err = body.error || JSON.stringify(body);
        return throwError(body);
        // return Observable.throw(body);
      }
    }
    else if (error.status === 404) {
      if (error instanceof Response) {
        //const err = body.error || JSON.stringify(body);
        return throwError(body);
        // return Observable.throw(body);
      }
    }
    // TODO handle 401 and other errors;
  }

  private handleSuccess(res: Response) {

    const body = res.json();
    return body || {};
  }

  downloadFile(data: Response, contentType: string, fileName: string) {
    const blob = new Blob([data.blob()], { type: contentType });
    const link = document.createElement('a');
    link.setAttribute("type", "hidden");
    link.href = window.URL.createObjectURL(blob);
    link.download = fileName;
    document.body.appendChild(link);
    link.click();
  
    setTimeout(()=>{link.remove()}, 1000);
  }

  private getContentType(fileName: string) {
    const extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    switch (extension) {
      case 'jpeg':
        return 'image/jpeg';
      case 'jpg':
        return 'image/jpeg';
      case 'png':
        return 'image/png';
      case 'gif':
        return 'image/gif';
      case 'bmp':
        return 'image/x-ms-bmp';
      case 'pdf':
        return 'application/pdf';
      case 'xls':
        return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }
    return '';
  }

  private onEnd(): void {
   
  }

  
}
 