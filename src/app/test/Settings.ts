import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable()
export class Settings {
 
    //Test Mode client 
    // public static ROZARPAY_API_KEY = "rzp_test_nJiOeQRIChS7A4"
    //Live Mode client 
    public static CART_DELETE_FLAG = "itemsPriceId";
    public static userArea = "areaArea";
    public static ROZARPAY_API_KEY = "rzp_live_Te542N1xWCm0dH"
    public static FRESH_AND_CLEANED_SEA_FOOD="8620d30ab3724253b5a9105e1c9c7bec";
    public static FRESH_AND_CLEANED_FRESH_WATER_FISH="0e204a189e854b008980ab3d331763ab";
    public static PRAWNS_AND_LOBSTER="7120d30ab3724253b5a9105e1c9c7bec";
    public static DRY_PRODUCTS="9220d30ab3724253b5a9105e1c9c7bec";
    static readonly DATE_FMT = 'dd/MM/yyyy';
    static readonly DATE_FMT_TIME = 'dd/MM/yyyy hh:mm:ss aa';
    public static USERDETAILS='userDetails';
    public static BASE_URL = environment.baseUrl;
    public static Auth_Token = 'AuthToken';
    public static SESSION_KEY = 'sessionKey';
    public static PINCODE = 'pincode';
 
    public static  ITEM_SORT_TYPE_HIGH_TO_LOW = 'highToLow';
	public static  ITEM_SORT_TYPE_LOW_TO_HIGH = 'lowToHigh';
	public static ITEM_SORT_TYPE_ALPHABETICAL = 'alphabetical';
   	public static  ITEM_REFINE_BY_LT20 = 'lt20';
	public static  ITEM_REFINE_BY_21TO50 = '21to50';
	public static  ITEM_REFINE_BY_50TO100 = '50to100';
    public static  ITEM_REFINE_BY_GT100 = 'gt100';
    public static  EXPRESS_CURRENT_TIME = new Date().toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
    public static  EXPRESS_AFTER_ONE_HOURS_TIME = new Date(new Date().getTime() + 1*60*60*1000).toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
    public static  EXPRESS_CURRENT_DATE = new Date(new Date().getTime() + 24*60*60*1000).toLocaleDateString();
}
