import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { RestApiService } from '../rest-api.service';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { StorageService } from '../storage.service';
import { Settings } from '../../Settings';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  pincode:any;
  area:any;
  currentUser: any;
  baseUrl = Settings.BASE_URL;
  private _inputSource = new Subject<string>();
  inputeMessage$ = this._inputSource.asObservable();
  private _inputUser = new Subject<string>();
  inputeUser$ = this._inputUser.asObservable();
  getUserDefaultAddress: any;

  constructor(private storageService : StorageService,
    private restApiService: RestApiService,
    // private tosterService: ToastrService,
    private http: HttpClient, 
    private route: Router) { }
 
  logIn(loginForm) {
    return this.http.post<any>(this.baseUrl + "/login", loginForm)
      .pipe(map(data => {
          if (data && data.sessionKey) {
          this.storageService.setItem(Settings.Auth_Token, data.sessionKey);
          this.storageService.setItem(Settings.USERDETAILS, data);             
        }
      }),
        catchError(this.handleError)
        
      )
  }
  isLoggedIn() {
    let user = localStorage.getItem(Settings.Auth_Token);
   
    if (user != null) {
    
      return true;
    } else {
    
      return false;
    }
  }
  getUserId() {
    if (this.isLoggedIn()) {
      this.currentUser = JSON.parse(localStorage.getItem(Settings.USERDETAILS));
      return this.currentUser.profile.userId;
    } else {
      return null;
    }
  }
    getFristName() {
    if (this.isLoggedIn()) {
      this.currentUser = JSON.parse(localStorage.getItem(Settings.USERDETAILS));
    
      return this.currentUser.profile.firstName;
    } else {
      return null;
    }
  }

  getLastName() {
    if (this.isLoggedIn()) {
      this.currentUser = JSON.parse(localStorage.getItem(Settings.USERDETAILS));
     
      return this.currentUser.profile.lastName;
    } else {
      return null;
    }
  }
  getPhoneNumber() {
    if (this.isLoggedIn()) {
      this.currentUser = JSON.parse(localStorage.getItem(Settings.USERDETAILS));
    
      return this.currentUser.profile.phoneNumber;
    } else {
      return null;
    }
  }
  getUserImage() {
    if (this.isLoggedIn()) {
      this.currentUser = JSON.parse(localStorage.getItem(Settings.USERDETAILS));
    
      return this.currentUser.profile.profileImage;
    } else {
      return null;
    }
  }
  sendMessage(message:string){
    this._inputSource.next(message);
  }
  sendUser(message:string){
    this._inputUser.next(message);
  }
  getAuthorizationToken() {
    return localStorage.getItem(Settings.Auth_Token);
  }

  logout() {
    localStorage.removeItem(Settings.Auth_Token);
    localStorage.removeItem(Settings.USERDETAILS);
    this.currentUser = null;
    localStorage.removeItem('isLoggedIn');
    this.route.navigateByUrl('/');
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error.error.message);
  }
}

