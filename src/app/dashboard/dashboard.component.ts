import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { windowCount } from 'rxjs/operators';
import { Settings } from '../test/Settings';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  currentUser:any;
  userDetails : any;
  tabId:any;
  constructor(private dashboardService :DashboardService,
    private router :Router) {   
     }

  ngOnInit() {
    let lasttabid= localStorage.getItem("lastTabId");
    let tabid= sessionStorage.getItem("tabId"); 
    let lasttabidint: number;
    console.log("lasttabid: "+lasttabid+" tabid: "+tabid);
    if(tabid === undefined || tabid == null || tabid.length<=0 || !tabid){
        if(lasttabid === undefined || lasttabid == null || lasttabid.length<=0 || !lasttabid){
            sessionStorage.setItem("tabId", "1");
            localStorage.setItem("lastTabId", "1");
            tabid= "1";
            this.tabId = sessionStorage.getItem("tabId"); 
        } else {
        
            lasttabidint= +lasttabid;
            console.log("lasttabidint "+lasttabidint)
            if(lasttabidint >1 ){
              alert('Already open!');
              this.router.navigate(['/']);
              }
        }
    }
this.tabId = tabid;
console.log("actual tab id is : "+ tabid);
    this.getUserDetails();
  }
  getUserDetails(){
    this.dashboardService.getUserDetails(this.tabId).subscribe(data =>{
      this.currentUser = data; 
    },error =>{
      alert('Already LoggedIn!');
    })
  }
 
}  
