import { Injectable } from '@angular/core';
import { RestApiService } from '../test/CommonServices/rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private restApi :RestApiService) { }

  getUserDetails(tabId){ 
     
    return this.restApi.get('user-details/'+tabId);
  }
}
