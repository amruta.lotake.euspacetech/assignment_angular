import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../test/CommonServices/auth/auth.service';
import { Settings } from '../test/Settings';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup; 
  loginStatus:boolean=false;
  constructor(   
    private _formBuilder: FormBuilder,
    private aroute: ActivatedRoute, 
    private route: Router,
    private authService: AuthService
    ) { 
      localStorage.removeItem(Settings.Auth_Token);
      localStorage.removeItem(Settings.USERDETAILS);
      this.loginStatus = this.authService.isLoggedIn();
      if(this.route.url === '/login' && this.loginStatus ){
        this.route.navigate(['/']);
      }
    }

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      email   : ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
  });
  }

  onSubmit() {
   this.authService.logIn(this.loginForm.value).subscribe(userDetails => {
     if (this.authService.isLoggedIn()) {
       this.route.navigate(['/dashboard']);
     }
   },error => {
     alert('Incorrect Username Or Password');
     }
   );
 }
get isLoggedIn() {
  return this.authService.isLoggedIn();
 }

}
